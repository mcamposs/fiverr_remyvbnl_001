const express = require('express');
const path = require('path');
const app = express();


app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, X-Authorization");
  next();
});


app.get('/', (req, res, next) => {
  res.status(200).sendFile(path.join(__dirname, './views/index.html'));
});

app.get('/profile', (req, res, next) => {
  res.status(200).sendFile(path.join(__dirname, './views/profile.html'));
});


app.listen(50451, () => {
  console.info('Running on port 50451');
});

app.use((err, req, res, next) => {
  switch (err.message) {
    case 'NoCodeProvided':
      return res.status(400).send({
        status: 'ERROR',
        error: err.message,
      });
    default:
      return res.status(500).send({
        status: 'ERROR',
        error: err.message,
      });
  }
});

// routes
app.use('/api/discord', require('./app/api/discord'));