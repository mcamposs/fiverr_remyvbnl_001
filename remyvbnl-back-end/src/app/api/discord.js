const express = require('express');
const entorno = require('../../config-module').config();
const fetch = require('node-fetch');
const btoa = require('btoa');
const { catchAsync } = require('../../utils');
var router = express.Router();

var bodyParser = require('body-parser');
router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());


const CLIENT_ID_DISCORD = entorno.CLIENT_ID_DISCORD;
const CLIENT_SECRET_DISCORD = entorno.CLIENT_SECRET_DISCORD;
const redirect = encodeURIComponent('http://localhost:50451/api/discord/callback');
const user_controller = require('../controllers/userController');

const mysql = require('mysql');

router.get('/login', (req, res, next) => {
  res.redirect(`https://discordapp.com/oauth2/authorize?client_id=${CLIENT_ID_DISCORD}&scope=identify&response_type=code&redirect_uri=${redirect}`);
});

/**
 * Enpoint user by id
 */
router.get('/users/:id', (req, res, next) => {
  console.log(req.params);

  const connection = mysql.createConnection({
    host: entorno.HOST_DB,
    database: entorno.DATABASE,
    user: entorno.USER_DB,
    password: entorno.PASSWORD_DB,
  });

  connection.connect(function (err) {
    if (err) {
      console.error('Error connecting: ' + err.stack);
      return;
    }

    console.log('Connected as is ' + connection.threadId);
  });

  var json = [];
  if (req.params.id != null) {
    connection.query(`SELECT * FROM user WHERE discord_id=${req.params.id} LIMIT 1;`,
      function (error, results, fields) {
        if (error)
          throw error;
        json = results[0];
        console.log(json);
        res.send(json);
      });

  }
  connection.end();
});

/**
 * Enpoint Verify
 */
router.post('/users/:idUser', (req, res, next) => {
  console.log(req.params.idUser);
  console.log(req);

  const connection = mysql.createConnection({
    host: entorno.HOST_DB,
    database: entorno.DATABASE,
    user: entorno.USER_DB,
    password: entorno.PASSWORD_DB,
  });

  connection.connect(function (err) {
    if (err) {
      console.error('Error connecting: ' + err.stack);
      return;
    }

    console.log('Connected as is ' + connection.threadId);
  });

  var json = [];
  if (req.body.rsn != null) {
    connection.query(`UPDATE user SET rsn = '${req.body.rsn}' WHERE user.id =${req.params.idUser};`,
      function (error, results, fields) {
        if (error)
          throw error;

        
        json = {
          'status': 'succes',
          'message': 'update user success'
        };
        console.log(json);
        res.send(json);
      });
      connection.query(`UPDATE user SET verified = 1 WHERE user.id =${req.params.idUser};`);

  }
  connection.end();
});


router.get('/profile', catchAsync(async (req, res) => {
  console.log(req.query);
  if (!req.query) throw new Error('NoCodeProvided');
  const token = req.query.token;
  const response = await fetch(`http://discordapp.com/api/users/@me`,
    {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  const json = await response.json();
  console.log(json);
  const connection = mysql.createConnection({
    host: entorno.HOST_DB,
    database: entorno.DATABASE,
    user: entorno.USER_DB,
    password: entorno.PASSWORD_DB,
  });


  connection.connect(function (err) {
    if (err) {
      console.error('Error connecting: ' + err.stack);
      return;
    }

    console.log('Connected as is ' + connection.threadId);
  });

  if (json.id != null) {
    connection.query(`INSERT INTO user (discord_id, username, locale, password, email, mfa_enabled, avatar, discriminator)
    SELECT * FROM (SELECT '${json.id}', '${json.username}', 
      '${json.locale}', '1234','jhondoe@email.com', ${json.mfa_enabled}, '${json.avatar}', ${json.discriminator}) AS tmp
    WHERE NOT EXISTS (
        SELECT discord_id FROM user WHERE discord_id = ${json.id}
    ) LIMIT 1;`, function (error, results, fields) {
        if (error)
          throw error;
        console.log(results);
      });
  }

  res.redirect(`http://localhost:4200/loading?token=${token}&discord_id=${json.id}&username=${json.username}&avatar=${json.avatar}`);
  connection.end();
  //res.send(json);
}));

router.get('/callback', catchAsync(async (req, res) => {
  if (!req.query.code) throw new Error('NoCodeProvided');
  const code = req.query.code;
  const creds = btoa(`${CLIENT_ID_DISCORD}:${CLIENT_SECRET_DISCORD}`);
  const response = await fetch(`https://discordapp.com/api/oauth2/token?grant_type=authorization_code&code=${code}&redirect_uri=${redirect}`,
    {
      method: 'POST',
      headers: {
        Authorization: `Basic ${creds}`,
      },
    });
  const json = await response.json();
  res.redirect(`profile?token=${json.access_token}`);
}));

module.exports = router;