import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ErrorStateMatcher } from '@angular/material';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { CustomValidator } from '../shared/validation';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    const invalid = !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
    const invalidCtrl = !!(control && control.invalid && control.dirty);
    const invalidParent = !!(control && control.parent && control.parent.invalid && control.parent.dirty);

    return (invalidCtrl || invalidParent || invalid);
  }
}
@Component({
  selector: 'app-verify',
  templateUrl: './verify.component.html',
  styleUrls: ['./verify.component.css']
})
export class VerifyComponent implements OnInit {

  url;
  user;
  url_avatar;
  disabled = false;
  myForm: FormGroup;
  matcher = new MyErrorStateMatcher();

  constructor(
    private userService: UserService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder
  ) {
    this.activatedRoute.url.subscribe(params => {
      this.url = params;
      console.log(true, this.url);
    });
  }

  ngOnInit() {

    this.myForm = this.fb.group({
      rsnFormControl: ['', [Validators.required, CustomValidator.regexValidator]]
   });


    if (this.url[1].path !== localStorage.getItem('username')) {
      this.router.navigate(['']);
    } else {
      this.userService.get(localStorage.getItem('discord_id')).subscribe(
        response => this.asignarUser(response)
      );
    }



    /*
     this.userService.getMe().subscribe(
       response => this.asignarUser(response)
     );
     */
  }

  submit(value: string) {
    // console.log(value);
    const req = {
      rsn: value
    };
    this.userService.verify(this.user.id, req).subscribe(
      response => window.alert(response.message)
    );

  }

  asignarUser(response) {
    this.user = response;
    this.myForm.get('rsnFormControl').setValue(this.user.rsn);
    this.url_avatar = `https://cdn.discordapp.com/avatars/${localStorage.getItem('discord_id')}/${localStorage.getItem('avatar')}.png`;
    // this.url_avatar = this.userService.getAvatarUrl(response.discord_id, response.avatar);
    localStorage.setItem('url_avatar', this.url_avatar);
  }
}
