import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const URL = 'http://localhost:50451/api/discord';
const CDN_BASE = 'https://cdn.discordapp.com';
@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private http: HttpClient
  ) { }

  get(id: String): Observable<any> {
    return this.http.get<any>(`${URL}/users/${id}`);
  }

  getMe(): Observable<any> {
    return this.http.get<any>(`${URL}/users/@me`);
  }


  getAvatarUrl(user_id, avatar): string {
    return `${CDN_BASE}/avatars/${user_id}/${avatar}.png`;
  }

  verify(user_id, rsn): Observable<any> {
    console.log(rsn);
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'X-Requested-With': 'XMLHttpRequest',
      })
    };
    return this.http.post<any>(`${URL}/users/${user_id}`, rsn, httpOptions);
  }

  getMeByDiscord(): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': `Bearer MlAbhtzr3ErFjN7IE8sA7B2EIlpO4T`,
        'Content-Type': `application/json`
     })
    };
    console.log(httpOptions);
    return this.http.get<any>('http://discordapp.com/api/users/@me', httpOptions);
  }
}
