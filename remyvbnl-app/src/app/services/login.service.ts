import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

const BASE_URL = 'https://discordapp.com/oauth2/authorize';
const CLIENT_ID = '493451057448353813';
const CLIENT_SECRET = 'U7RvHD_vV3hDVYjhSc5Rg2OzB_uAS8OV';
const REDIRECT = encodeURIComponent('http://localhost:50451/api/discord/callback');

@Injectable({
  providedIn: 'root'
})

export class LoginService {

  constructor(
    private http: HttpClient,
  ) { }

  loginFromDiscord() {
    return this.http.get(`${BASE_URL}?client_id=${CLIENT_ID}&scope=identify&response_type=code&redirect_uri=${REDIRECT}`);
  }
}
