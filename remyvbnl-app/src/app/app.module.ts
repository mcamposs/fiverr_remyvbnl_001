import { BrowserModule } from '@angular/platform-browser';
import { ParticlesModule } from 'angular-particle';
import { NgModule } from '@angular/core';
import { MaterialModule } from './material.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login/login.component';
import {CdkTableModule} from '@angular/cdk/table';
import {CdkTreeModule} from '@angular/cdk/tree';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ProfileComponent } from './profile/profile.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { TableComponent } from './table/table.component';
import { CustomProgressBarComponent } from './custom-progress-bar/custom-progress-bar.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { APP_BASE_HREF } from '@angular/common';
import { LoadingComponent } from './loading/loading.component';
import { TokenInterceptor } from './interceptor/token.interceptor';
import { VerifyComponent } from './verify/verify.component';

const appRoutes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'profile/:username',
    component: ProfileComponent
  },
  {
    path: 'verify/:username',
    component: VerifyComponent
  },
  {
    path: 'table',
    component: TableComponent
  },
  {
    path: 'loading',
    component: LoadingComponent
  },
  {
    path: '**',
    component: PageNotFoundComponent
  },
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ProfileComponent,
    HeaderComponent,
    HomeComponent,
    TableComponent,
    CustomProgressBarComponent,
    PageNotFoundComponent,
    LoadingComponent,
    VerifyComponent
  ],
  imports: [
    CdkTableModule,
    CdkTreeModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    MaterialModule,
    BrowserAnimationsModule,
    ParticlesModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true }// <-- debugging purposes only)
     )
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
