import { Component, OnInit } from '@angular/core';
import { LoginService } from '../services/login.service';
import { UserService } from '../services/user.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  url;
  user;
  url_avatar;
  disabled = false;

  constructor(
    private userService: UserService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    this.activatedRoute.url.subscribe(params => {
      this.url = params;
      console.log(true, this.url);
    });
  }

  ngOnInit() {
    if (this.url[1].path !== localStorage.getItem('username')) {
      this.router.navigate(['']);
    } else {
      this.userService.get(localStorage.getItem('discord_id')).subscribe(
        response => this.asignarUser(response)
      );
    }


   /*
    this.userService.getMe().subscribe(
      response => this.asignarUser(response)
    );
    */
  }

  back() {
    this.router.navigate(['']);
  }

  asignarUser(response) {
    this.user = response;
    this.url_avatar = `https://cdn.discordapp.com/avatars/${localStorage.getItem('discord_id')}/${localStorage.getItem('avatar')}.png`;
    // this.url_avatar = this.userService.getAvatarUrl(response.discord_id, response.avatar);
    localStorage.setItem('url_avatar', this.url_avatar);
  }

}
