import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../../services/login.service';

declare var particle: any;
const BASE_URL = 'https://discordapp.com/oauth2/authorize';
const CLIENT_ID = '493451057448353813';
const CLIENT_SECRET = 'U7RvHD_vV3hDVYjhSc5Rg2OzB_uAS8OV';
const REDIRECT = encodeURIComponent('http://localhost:50451/api/discord/callback');

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private loginService: LoginService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  loginFromDiscord() {
    window.location.href = `http://localhost:50451/api/discord/login`;
  }

}
