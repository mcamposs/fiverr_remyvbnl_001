import { Component, OnInit } from '@angular/core';
export interface User {
  pos: number;
  username: string;
  discriminator: string;
  xp_current: number;
  xp_lvl: number;
  xp_total: number;
  lvl: number;
}
const USER_DATA: User[] = [
  {pos: 1, username: 'Dragon Godness', discriminator: '#5464', xp_current: 1600, xp_lvl: 1700, xp_total: 3000, lvl: 55},
  {pos: 2, username: 'beal', discriminator: '#5214', xp_current: 1400, xp_lvl: 1600, xp_total: 3323, lvl: 54},
  {pos: 3, username: 'Tobiasaw', discriminator: '#321', xp_current: 1000, xp_lvl: 1400, xp_total: 3323, lvl: 52},
  {pos: 4, username: 'Dj', discriminator: '#4345', xp_current: 784, xp_lvl: 1200, xp_total: 2323, lvl: 50},
  {pos: 5, username: 'HIV', discriminator: '#7678', xp_current: 100, xp_lvl: 900, xp_total: 3000, lvl: 47},
  {pos: 6, username: 'Mamoh', discriminator: '#0867', xp_current: 12, xp_lvl: 800, xp_total: 3000, lvl: 46},
  {pos: 7, username: 'narick', discriminator: '#3483', xp_current: 2, xp_lvl: 700, xp_total: 3000, lvl: 45},
  {pos: 8, username: 'wufled', discriminator: '#9434', xp_current: 399, xp_lvl: 400, xp_total: 3000, lvl: 42},
  {pos: 9, username: 'sstarlod', discriminator: '#0854', xp_current: 300, xp_lvl: 350, xp_total: 3000, lvl: 41},
  {pos: 10, username: 'teh now', discriminator: '#3878', xp_current: 200, xp_lvl: 340, xp_total: 3000, lvl: 39},
];
@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {

  dataSource = USER_DATA;
  constructor() { }

  ngOnInit() {
  }

  porcentaje(xp_current, xp_lvl) {
    return xp_current / xp_lvl * 100;
  }
}
