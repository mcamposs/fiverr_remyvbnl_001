import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.css']
})
export class LoadingComponent implements OnInit, AfterViewInit {

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private userService: UserService
  ) {
    this.activatedRoute.queryParams.subscribe(params => {
      localStorage.setItem('token', params['token']);
      localStorage.setItem('discord_id', params['discord_id']);
      localStorage.setItem('username', params['username']);
      localStorage.setItem('avatar', params['avatar']);
    });
  }
  ngOnInit() {
    if (localStorage.getItem('discord_id') !== null) {
      this.router.navigate([`profile/${localStorage.getItem('username')}`]);
    } else {
      this.router.navigate(['PageNotFound']);
    }

  }

  ngAfterViewInit() {
    if (localStorage.getItem('token') !== null) {

    }

  }
  setItemLocalStorage(key, item) {
    localStorage.setItem(key, item);
  }
}
