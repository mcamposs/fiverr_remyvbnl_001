import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-custom-progress-bar',
  templateUrl: './custom-progress-bar.component.html',
  styleUrls: ['./custom-progress-bar.component.css']
})
export class CustomProgressBarComponent implements OnInit {
  @Input() user;
  constructor() { }

  ngOnInit() {
  }

  porcentaje(xp_current, xp_lvl) {
    return xp_current / xp_lvl * 100;
  }
}
