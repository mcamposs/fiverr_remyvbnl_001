import { Component, OnInit, Input } from '@angular/core';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  url_avatar;
  username;
  constructor(
    private userService: UserService
  ) {
    this.url_avatar = `https://cdn.discordapp.com/avatars/${localStorage.getItem('discord_id')}/${localStorage.getItem('avatar')}.png`;
    this.username = localStorage.getItem('username');
   }

  ngOnInit() {

  }

}
